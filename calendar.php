<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ./register_login_system/login.php");
    exit;
}
?>

<?php
$title_date=array();
$title=array();
$title_id=array();
$title_start=array();
$title_end=array();

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "calendar";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$user_id=$_SESSION['id'];
$sql = "SELECT app_id,start,end,app,date FROM appointment WHERE user_id='$user_id' ORDER BY start ASC";
$result = mysqli_query($conn, $sql);


while($row = mysqli_fetch_assoc($result)) {
    array_push($title_date, $row["date"]);
    array_push($title, $row["app"]);
    array_push($title_id, $row["app_id"]);
    array_push($title_start, $row["start"]);
    array_push($title_end, $row["end"]);
}

mysqli_close($conn);
?>

<html>
<head> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/month_style.css">
</head>
<body class="body">

<?php 

if(isset($_REQUEST['date'])){ //check if it has sent date value
    $day = date('d', strtotime($_REQUEST['date']));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime($_REQUEST['date']));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime($_REQUEST['date']));     //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime('01-'.$month .'-'. $year));  //Gets the day of the week for the 1st of  
                    //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime($_REQUEST['date']));      //Gets number of days in month 
    
    $today = date('d');            //Gets today’s date 
    $todaymonth = date('m');          //Gets today’s month 
    $todayyear = date('Y');            //Gets today’s year 
    $monthname=date("F", strtotime($_REQUEST['date']));
    $nmonth = strtotime($_REQUEST['date']); 
   
}else{ //use current date
    $day = date('d', strtotime(date("Y-m-d")));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime(date("Y-m-d")));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime(date("Y-m-d")));     //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime('01-'.$month .'-'. $year));  //Gets the day of the week for the 1st of  
                    //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime(date("Y-m-d")));      //Gets number of days in month 
    $today = date('d');            //Gets today’s date 
    $todaymonth = date('m');          //Gets today’s month 
    $todayyear = date('Y');            //Gets today’s year 
    $monthname=date("F", strtotime(date("Y-m-d")));
    $nmonth = strtotime(date("Y-m-d"));
}
$nextmonth = date('Y-m-d', strtotime('+1 month', $nmonth));
$prevmonth = date('Y-m-d', strtotime('-1 month', $nmonth));
$this_date = $year."-".$month."-".$day;
?> 
    
    <button class="openbtn" onclick="openNav()" title="MENU">☰</button>
    <div id="sidebar-wrapper" onclick="closeNav()"></div>
    <div id="mySidebar" class="sidebar">
        <a class="closebtn" onclick="closeNav()">×</a>
        <div id = "username">           
            <?php
               echo $_SESSION["username"];
            ?>

        </div> 
        <hr id = "hr">
        <a href="./calendar_day.php?date=<?php echo $this_date; ?>">Day View</a>
        <a href="./calendar_week.php?date=<?php echo $this_date; ?>">Week View</a>
        <a href="#">Month View</a>
        <a href = "./register_login_system/logout.php">Logout</a>
    </div>

    <div id="app-bar" class="sidebar" style="overflow:auto; color:white;">
        <div id="appointment-date"></div>
        <hr id = "hr">

        <form method="post" action="./appointment/addapp.php" style="color:white;">
            title: <input type="text" name="appoint_text" style="width:80%;"><br>
            start:<input type="time" name="appoint_start"><br>
            end  : <input type="time" name="appoint_end"><br>
            <input type="date" name="appoint_date" id ="appoint_date" hidden><br>
            <input type="submit" value="SUBMIT" >
        </form>

        <br>
        <div id="app_content"></div>
    </div>

    <a href = "?date=<?php  echo $prevmonth; ?>">
        <div class="backicon">
        <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
            <path fill-rule="evenodd" d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z"/>
            <path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z"/>
        </svg>
        </div>
    </a>

    <a href = "?date=<?php  echo $nextmonth; ?>">
        <div class="nexticon">
        <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
            <path fill-rule="evenodd" d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z"/>
            <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z"/>
        </svg>
        </div>
    </a>
         <center class="monthname">
            <?php echo $monthname ?> <br>
            <a href = "http://localhost/calendar/calendar"> <!-- get back to current date -->
            <button type="button" class="today-btn btn btn-outline-warning">TODAY</button>
            </a>
        </center>

        <div class="calendar"> 
            <div class="days">Sunday</div> 
            <div class="days">Monday</div> 
            <div class="days">Tuesday</div> 
            <div class="days">Wednesday</div> 
            <div class="days">Thursday</div> 
            <div class="days">Friday</div> 
            <div class="days">Saturday</div> 

            <?php 
                //create blankday box
                for($i=1; $i<=$firstday; $i++) 
                { 
                    echo '<div class="date blankday"></div>'; 
                } 
                //create day box in month
                for($i=1; $i<=$days; $i++) 
                {   
                    if($i<10){
                        $this_date=$year."-".$month."-0".$i; //date of each box
                    }else{
                        $this_date=$year."-".$month."-".$i; //date of each box
                    }

                    $app_date ="'".$this_date."'";
                   
                    echo '<div onclick="openApp('.$app_date.')" class="date'; 
                    //show today
                    if ($today == $i && $todaymonth==$month && $todayyear == $year) 
                    { 
                        echo ' today'; 
                    } 
                    echo '" style="white-space: nowrap;
                    overflow: hidden;
                    text-overflow: ellipsis; ">' . $i . '<br>'; 
                    //show appointment
                    for($j=0;$j<sizeof($title);$j++){
                        if($this_date == $title_date[$j]){
                            echo "●";
                        }
                    }
                    
                    echo  '</div>'; 
                } 
                //create left blankday box
                $daysleft = 7-(($days + $firstday)%7); 
                if($daysleft<7) 
                { 
                    for($i=1; $i<=$daysleft; $i++) 
                    { 
                        echo '<div class="date blankday"></div>'; 
                    } 
                } 
            ?> 

        </div>
        <div class="month">
            <?php
                echo $month;
            ?>
        </div>

        <div class="year">
            <?php
                echo $year;
            ?>
        </div>

    </body>

<script>
    function openNav() {
        document.getElementById("mySidebar").style.width = "250px";
        document.getElementById("sidebar-wrapper").style.display = "block";
    }

    function closeNav() {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("app-bar").style.width = "0";
        document.getElementById("sidebar-wrapper").style.display = "none";
    }

    function openApp(date) {
        let title_date = [];
        title_date.push(
            <?php
                for($j=0;$j<sizeof($title);$j++){
                    if($j!=sizeof($title)-1){
                        echo "'". $title_date[$j]."',";
                    }else{
                        echo "'". $title_date[$j]."'";
                    }
                    
                }
                
            ?>
        );
        let title = [];
        title.push(
            <?php
                for($j=0;$j<sizeof($title);$j++){
                    if($j!=sizeof($title)-1){
                        echo "'". $title[$j]."',";
                    }else{
                        echo "'". $title[$j]."'";
                    }
                    
                }
                
            ?>
        );

        let title_id = [];
        title_id.push(
            <?php
                for($j=0;$j<sizeof($title);$j++){
                    if($j!=sizeof($title)-1){
                        echo "'". $title_id[$j]."',";
                    }else{
                        echo "'". $title_id[$j]."'";
                    }
                    
                }
                
            ?>
        );

        let title_start = [];
        title_start.push(
            <?php
                for($j=0;$j<sizeof($title);$j++){
                    if($j!=sizeof($title)-1){
                        echo "'". $title_start[$j]."',";
                    }else{
                        echo "'". $title_start[$j]."'";
                    }
                    
                }
                
            ?>
        );
        
        let title_end = [];
        title_end.push(
            <?php
                for($j=0;$j<sizeof($title);$j++){
                    if($j!=sizeof($title)-1){
                        echo "'". $title_end[$j]."',";
                    }else{
                        echo "'". $title_end[$j]."'";
                    }
                    
                }
                
            ?>
        );

        document.getElementById("app-bar").style.width = "250px";
        document.getElementById("sidebar-wrapper").style.display = "block";
        document.getElementById("appointment-date").innerHTML = date;
        document.getElementById("appoint_date").value = date;
        for(var i=0;i<title.length;i++){
            if(date==title_date[i]){
                document.getElementById("app_content").innerHTML += 
                "<div class='appointment'>"+
                "   <b>"+title[i]+"</b><br>"+
                "   start: "+title_start[i]+"<br>end: "+title_end[i]+
                "</div>"+
                "<form action='./appointment/delete_app' method='POST'>"+
                "   <input type ='text' name='app_id' value='"+title_id[i]+"'hidden>"+
                "   <input type ='text' name='app_date' value='"+title_date[i]+"'hidden>"+
                "   <input type='submit' value='delete' type='button' class='btn btn-outline-danger'>"+
                "</form><br><br>";
            }       
        }
        
    }
</script>

</html>