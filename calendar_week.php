<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: ./register_login_system/login.php");
    exit;
}
?>

<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "calendar";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}

$user_id=$_SESSION['id'];

?>

<?php 

if(isset($_REQUEST['date'])){ //check if it has sent date value
    $day = date('d', strtotime($_REQUEST['date']));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime($_REQUEST['date']));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime($_REQUEST['date']));     //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime('01-'.$month .'-'. $year));  //Gets the day of the week for the 1st of  
                    //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime($_REQUEST['date']));      //Gets number of days in month 
    $monthname=date("F", strtotime($_REQUEST['date']));
    $nmonth = strtotime($_REQUEST['date']); 
   
}else{ //use current date
    $day = date('d', strtotime(date("Y-m-d")));      //Gets day of appointment (1‐31) 
    $month = date('m', strtotime(date("Y-m-d")));      //Gets month of appointment (1‐12) 
    $year = date('Y', strtotime(date("Y-m-d")));     //Gets year of appointment (e.g. 2016) 
    $firstday = date('w', strtotime('01-'.$month .'-'. $year));  //Gets the day of the week for the 1st of  
                    //the month. (e.g. 0 for Sun, 1 for Mon) 
    $days = date('t', strtotime(date("Y-m-d")));      //Gets number of days in month 
    $monthname=date("F", strtotime(date("Y-m-d")));
    $nmonth = strtotime(date("Y-m-d"));
}

$this_date = $year."-".$month."-".$day;
$minus = $day+1;
$plus = $days-$day+1;

$firstday_week=array();
for($i=1; $i<=$days;$i++){
    if($i%7==1){
        array_push($firstday_week,$i);
    }
}
        
$weekindex=0; 
for($i=0;$i<sizeof($firstday_week);$i++){
    if($day>=$firstday_week[$i]){
        $weekindex=$i;
    }
}
$day=$firstday_week[$weekindex];

if($weekindex<sizeof($firstday_week)-1){
    $nextweek = date("Y-m-d",strtotime("+7 day", $nmonth ));             
}else{
    $nextweek = date("Y-m-d",strtotime("+$plus day", $nmonth ));
}

if($weekindex==0){
    $prevweek = date("Y-m-d",strtotime("-$minus day",  $nmonth));
}else{
    $prevweek = date("Y-m-d",strtotime("-7 day",  $nmonth));
}




?>

<html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="./css/week_view.css">
</head>

<body class="body">


    <button class="openbtn" onclick="openNav()" title="MENU">☰</button>
    <div id="sidebar-wrapper" onclick="closeNav()"></div>
    <div id="mySidebar" class="sidebar">
        <a class="closebtn" onclick="closeNav()">×</a>
        <div id="username">
            <?php
               echo $_SESSION["username"];
            ?>

        </div>
        <hr id="hr">
        <a href="./calendar_day.php?date=<?php echo $this_date; ?>">Day View</a>
        <a href="#">Week View</a>
        <a href="./calendar.php?date=<?php echo $this_date; ?>">Month View</a>
        <a href="./register_login_system/logout.php">Logout</a>
    </div>

    <center><div class="monthname"><?php echo $monthname." ".$year; ?></div></center>
    <center><div class="addapp">
    <form action="./appointment/addapp_week.php" method="POST">
            Title: <input type="text" name="appoint_text">
            Date: <input type="date" name="appoint_date">
            Start: <input type="time" name="appoint_start">
            End: <input type="time" name="appoint_end">
            <input type="submit" class="btn btn-warning" value="SUBMIT">
        </form>
    
    </div></center>
    

    <div class="prevweek">
    <a href="?date=<?php  echo $prevweek; ?>">
                <div class="backicon">
                    <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-arrow-left-circle"
                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path fill-rule="evenodd"
                            d="M8.354 11.354a.5.5 0 0 0 0-.708L5.707 8l2.647-2.646a.5.5 0 1 0-.708-.708l-3 3a.5.5 0 0 0 0 .708l3 3a.5.5 0 0 0 .708 0z" />
                        <path fill-rule="evenodd" d="M11.5 8a.5.5 0 0 0-.5-.5H6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 .5-.5z" />
                    </svg>
                </div>
            </a>
    
    </div>

    <div class="nextweek">
    <a href="?date=<?php  echo $nextweek; ?>">
                <div class="nexticon">
                    <svg width="3em" height="3em" viewBox="0 0 16 16" class="bi bi-arrow-right-circle"
                        fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                            d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path fill-rule="evenodd"
                            d="M7.646 11.354a.5.5 0 0 1 0-.708L10.293 8 7.646 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0z" />
                        <path fill-rule="evenodd" d="M4.5 8a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5z" />
                    </svg>
                </div>
            </a>
    
    </div>

    <div class="showapp">
        <?php
            $a=$day;
            for($d=1;$d<=7 ;$d++){
                if($a<=$days){
                    $get_apm_date = "$year-$month-$a";
                    $dayname = date('D', strtotime($get_apm_date));
                    $title=array();
                    $title_id=array();
                    $title_start=array();
                    $title_end=array();
                    $sql = "SELECT app_id,start,end,app FROM appointment WHERE user_id='$user_id' AND date='$get_apm_date' ORDER BY start ASC";
                    $result = mysqli_query($conn, $sql);


                    while($row = mysqli_fetch_assoc($result)) {
                        array_push($title, $row["app"]);
                        array_push($title_id, $row["app_id"]);
                        array_push($title_start, $row["start"]);
                        array_push($title_end, $row["end"]);
                    }
        ?>
                        
            <?php 
                echo '<div class="appoint">
                        <div class="showday">
                            <div class="day-box">
                                <div class="day">'.$a.'</div>
                                <div class="day-name">'.$dayname.'</div>
                            </div>
                        </div>';
                    for($x=0; $x<sizeof($title);$x++){
                        echo "<div class='appbox'>
                                <form class='delete-app' action='./appointment/delete_app_week' method='POST'>
                                    <input type ='text' name='app_id' value='".$title_id[$x]."'hidden>
                                    <input type ='text' name='app_date' value='".$get_apm_date."'hidden>
                                </form>
                                <div onclick='delete_app(".$x.")' class='bin'>
                                <svg width='1em' height='1em' viewBox='0 0 16 16' class='bi bi-trash-fill' fill='currentColor' xmlns='http://www.w3.org/2000/svg'>
                                <path fill-rule='evenodd' d='M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z'/>
                                </svg>
                                </div>
                                <h2>".$title[$x]."</h2> 
                                <hr>
                                ".$title_start[$x]."-".$title_end[$x]."
                        </div>";
                    } 
                echo '</div>';
                $a++;
                }

            }           
        ?>
    </div>

</body>

<script>
function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("sidebar-wrapper").style.display = "block";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("sidebar-wrapper").style.display = "none";
}

function delete_app(index){
    document.getElementsByClassName("delete-app")[index].submit();
}
</script>

</html>

<?php 
    mysqli_close($conn);
?>